#!/usr/bin/python
import json
import psycopg2
from psycopg2 import sql
from psycopg2.extras import Json
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import pandas

def main():
    # connect to default postgres database
    connection = psycopg2.connect(database="postgres", user='postgres', password='password', host='127.0.0.1', port= '5432')
    connection.autocommit = True
    cursor = connection.cursor()

    # removing table if already exist, for testing purposes
    try:
        cursor.execute('DROP TABLE interfaces')
    except:
        pass

    # define new table in default postgres database
    make_table = """CREATE TABLE interfaces(id SERIAL PRIMARY KEY,
                                            connection INTEGER,
                                            name VARCHAR(255) NOT NULL,
                                            description VARCHAR(255),
                                            config json,
                                            type VARCHAR(50),
                                            infra_type VARCHAR(50),
                                            port_channel_id INTEGER,
                                            max_frame_size INTEGER)"""

    # make table
    cursor.execute(make_table)

    # loading data
    with open('configClear_v2.json') as file:
        plain = json.load(file)
        file.close()
        data = plain['frinx-uniconfig-topology:configuration']['Cisco-IOS-XE-native:native']['interface']
   
   # looping through interfaces
    for interface_type in data.keys():
        # ignoring BDIs and Loopbacks
        if str(interface_type) == 'BDI' or str(interface_type) == 'Loopback':
            pass
        else:
            for interfaces in range (0, len(data[interface_type])):
                # extracting name
                name = data[interface_type][interfaces]['name']
                if str(interface_type) == 'Port-channel':
                    name = str(interface_type) + str(name)
                elif str(interface_type) == 'TenGigabitEthernet':
                    name = str(interface_type) + str(name)
                else:
                    name = str(interface_type) + str(name)
            
                # extracting description
                try:
                    description = str(data[interface_type][interfaces]['description'])
                except:
                    description = None
                       
                # extracting mtu
                try:
                    mtu = data[interface_type][interfaces]['mtu']
                except:
                    mtu = None
            
                # extracting config data
                config = Json(data[interface_type][interfaces])
            
                # linking ethernet interfaces to port channels
                # only works if port channels are before ethernet interfaces
                if str(interface_type) != 'Port-channel':
                    try:
                        port_id = data[interface_type][interfaces]['Cisco-IOS-XE-ethernet:channel-group']['number']
                        port_id_name = 'Port_channel' + str(port_id)
                        try:    
                            cursor.execute("""SELECT id FROM interfaces WHERE name LIKE %s;""",[port_id_name])
                            port = cursor.fetchall()
                            port_channel_id = port[0][0]
                        except:
                            port_channel_id = None
                    except:
                        port_channel_id = None
                else:
                    port_channel_id = None
            
                # inserting data to table
                cursor.execute("""INSERT INTO interfaces (name, description, max_frame_size, config, port_channel_id)
                                VALUES (%s, %s, %s, %s, %s);""",[name, description, mtu, config, port_channel_id])

    # printing table
    my_table = pandas.read_sql('SELECT id, name, description, config, port_channel_id, max_frame_size FROM interfaces', connection)
    print(my_table)

if __name__ == '__main__':
    main()
